$(window).scroll(function() {
  if ($(this).scrollTop() > 50) {
      $('.logo').addClass('animated-logo');
  } else {
      $('.logo').removeClass('animated-logo');
  }
});

window.onscroll = function() {
  var theta = document.documentElement.scrollTop / 100 % Math.PI;
document.getElementById('photo-animate2').style.transform ='rotate(' + theta + 'rad)';
}
var $sun991 = $('.none4');
var $win991 = $(window);
$win991.on('scroll', function () {
  var top = $win991.scrollTop() / 1050  ;
  $sun991.css('transform', `rotate(-${top^10}deg)`);
});

var $sun1 = $('#photo-animate3');
var $win1 = $(window);
$win1.on('scroll', function () {
  var top = $win1.scrollTop() / 15;
  $sun1.css('transform', `rotate(${--top}deg)`);
  $sun1.css(`opacity`, `${top/110^0}`);
});
var $sun19 = $('.none5');
var $win19 = $(window);
$win19.on('scroll', function () {
  var top = $win19.scrollTop() / 15;
  $sun19.css('transform', `rotate(${--top}deg)`);
  $sun19.css(`opacity`, `${top/110^0}`);
});

var $sun2 = $('#photo-animate5');
var $win2 = $(window);
$win2.on('scroll', function () {
  var top = $win2.scrollTop() / 15;
  $sun2.css('transform', `rotate(${--top}deg)`);
  $sun2.css(`opacity`, `${top/100^1}`);
});
var $sun22 = $('.none8');
var $win22 = $(window);
$win22.on('scroll', function () {
  var top = $win22.scrollTop() / 15;
  $sun22.css('transform', `rotate(${--top}deg)`);
  $sun22.css(`opacity`, `${top/100^1}`);
});

var $animObject = $('#photo-animate7');
var $window = $(window);
$window.on('scroll', function() {
  var fromTop = $window.scrollTop() / -50;
  $animObject.css(`transform` , `rotate(${fromTop+75}deg)  matrix(${fromTop/-70}, ${fromTop/150}, ${fromTop/-150}, ${fromTop/-70}, 0, 0)`);
});
var $animObject1 = $('.none10');
var $window1 = $(window);
$window1.on('scroll', function() {
  var fromTop = $window1.scrollTop() / -140;
  $animObject1.css('transform', `rotate(${fromTop^10}deg)`)
});

var $sun5 = $('#photo-animate11');
var $win5 = $(window);
$win5.on('scroll', function () {
  var top = $win5.scrollTop() / 10 ;
  $sun5.css(`opacity`, `${top/115^3}`);
});
var $sun51 = $('.none9');
var $win51 = $(window);
$win51.on('scroll', function () {
  var top = $win51.scrollTop() / 10 ;
  $sun51.css(`opacity`, `${top/100^4}`);
});

var $sun6 = $('.top');
 var $win6 = $(window);
 $win6.on('scroll', function () {
  var top = $win6.scrollTop() / 7.5;
  $sun6.css('transform', `translateY(-${top}px)`).stop();
 $sun6.css(`opacity`, `${top/250 + 0.0025}`);
 $(this).animate({scrollTop:$(".top").offset().top},"fadeIn(1000) , opacity:0.15");
 });
 var $sun61 = $('.none1');
 var $win61 = $(window);
 $win61.on('scroll', function () {
  var top = $win61.scrollTop() / 7;
  $sun61.css('transform', `translateY(-${top}px)`).stop();
 $sun61.css(`opacity`, `${top/250 + 0.0025}`);
 $(this).animate({scrollTop:$(".top").offset().top},"fadeIn(1000) , opacity:0.15");
 });

 var $sun7 = $('#photo-animate61');
 var $win7 = $(window);
 $win7.on('scroll', function () {
  var top = $win7.scrollTop() / 5;
  $sun7.css(`transition`, `ease-in (${top++}s) 1s`);
 $sun7.css(`transform`, `rotate(-${top/20}deg) matrix3d(${top/700}, -${top/900}, 0, 0, ${top/900}, ${top/700}, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)`);
 $sun7.css(`opacity`, `${top/100^4}`);
 });
 var $sun71 = $('.none61');
 var $win71 = $(window);
 $win71.on('scroll', function () {
  var top = $win71.scrollTop() / 5;
  $sun71.css(`transition`, `ease-in (${top++}s) 1s`);
 $sun71.css(`transform`, `rotate(-${top/20}deg) matrix3d(${top/900}, -${top/1100}, 0, 0, ${top/1100}, ${top/900}, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)`);
 $sun71.css(`opacity`, `${top/100^4}`);
 });

 var $sun8 = $('#photo-animate62');
 var $win8 = $(window);
 $win8.on('scroll', function () {
  var top = $win8.scrollTop() / 4;
  $sun8.css(`transition`, `ease-in (${top/1}s) 1s`);
 $sun8.css(`transform`, `rotate(${top/20}deg) matrix3d(${top/700}, -${top/900}, 0, 0, ${top/900}, ${top/700}, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)`);
 $sun8.css(`opacity`, `${top/200^2}`);
 });
 var $sun81 = $('.none62');
 var $win81 = $(window);
 $win81.on('scroll', function () {
  var top = $win81.scrollTop() / 4;
  $sun81.css(`transition`, `ease-in (${top/1}s) 1s`);
 $sun81.css(`transform`, `rotate(${top/20}deg) matrix3d(${top/900}, -${top/1100}, 0, 0, ${top/1100}, ${top/900}, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)`);
 $sun81.css(`opacity`, `${top/100^4}`);
 });

 var $sun9 = $('#photo-animate63');
 var $win9 = $(window);
 $win9.on('scroll', function () {
  var top = $win9.scrollTop() / 3;
  $sun9.css(`transition`, `ease-in (${top/1}s) 1s`);
 $sun9.css(`transform`, `rotate(${top/30}deg) matrix3d(${top/800}, -${top/900}, 0, 0, ${top/900}, ${top/800}, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)`);
 $sun9.css(`opacity`, `${top/300^2}`);
 });
 var $sun91 = $('.none63');
 var $win91 = $(window);
 $win91.on('scroll', function () {
  var top = $win91.scrollTop() / 3;
  $sun91.css(`transition`, `ease-in (${top/1}s) 1s`);
 $sun91.css(`transform`, `rotate(${top/30}deg) matrix3d(${top/900}, -${top/1100}, 0, 0, ${top/1100}, ${top/900}, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)`);
 $sun91.css(`opacity`, `${top/100^4}`);
 });

 var $sun3 = $('#photo-animate64');
var $win3 = $(window);
$win3.on('scroll', function () {
  var top = $win3.scrollTop() / 11  ;
  $sun3.css('transform', `rotate(${top}deg) 
`);
  $sun3.css(`opacity`, `${top/30^6}`);
});
var $sun33 = $('.none64');
var $win33 = $(window);
$win33.on('scroll', function () {
  var top = $win33.scrollTop() / 11  ;
  $sun33.css('transform', `rotate(${top}deg)
`);
  $sun33.css(`opacity`, `${top/30^6}`);
});

// ANIMATION

AOS.init({
  duration: 1200,
})
